<!doctype html>
<html class="no-js" lang="en">
<?php include '_partials/head.php'; ?>
<body>
    <!--[if lt IE 9]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <?php include '_partials/header.php'; ?>
    <main class="site-main">
        <div class="container">
            <div class="bzg">
                <div class="bzg_c border-right" data-col="m9">
                    <article class="forum">
                        <h1 class="header-large">Lampu webcam tiba-tiba menyala sendiri tanpa membuka aplikasi webcam</h1>
                        <section class="forum-content">
                            <p>Mau tanya, akhir-akhir ini webcam sering nyala sendiri. Apakah ada yang tahu penyebabnya dan solusi untuk memperbaiki hal itu? Apakah ada kemungkinan lapot saya di-hack karena kasus terjadi tiap terkoneksi di internet</p>
                        </section>
                        <section class="forum-replies">
                            <h3 class="header-medium linetrough">Komentar</h3>
                            <template id="komentar">
                                {{#each this}}
                                <article class="bzg_c forum-reply" data-col="s12">
                                    <section class="media">
                                        <img class="media-figure media-figure--small" src="{{avatar}}" alt="">
                                        <div class="media-content">
                                            <h4 class="header-xsmall">{{author}}</h4>
                                            <time class="small">{{date}}</time>
                                            <p>{{{message}}}</p>
                                            <div class="vote-content">
                                                <span class="count">{{point}}</span> point
                                                <a class="voting upvote"><i class="fa fa-arrow-up fa-fw"></i></a>
                                                <a class="voting downvote"><i class="fa fa-arrow-down fa-fw"></i></a>
                                            </div>
                                            {{#each replies}}
                                            <article class="bzg forum-reply-comment">
                                                <section class="bzg_c media" data-col="s12">
                                                    <img class="media-figure media-figure--smaller" src="{{avatar}}" alt="">
                                                    <div class="media-content">
                                                        <h4 class="header-xsmall">{{author}}</h4>
                                                        <time class="small">{{date}}</time>
                                                        <p>{{{message}}}</p>
                                                        <div class="vote-content">
                                                            <span class="count">{{point}}</span> point
                                                            <a class="voting upvote"><i class="fa fa-arrow-up fa-fw"></i></a>
                                                            <a class="voting downvote"><i class="fa fa-arrow-down fa-fw"></i></a>
                                                        </div>
                                                    </div>
                                                </section>
                                            </article>
                                            {{/each}}
                                        </div>
                                    </section>
                                </article>
                                {{/each}}
                            </template>
                            <div class="bzg list-komen" data-content="comments.json"></div>
                        </section>
                        <section>
                            <h3 class="header-small linetrough">Tambahkan komentar</h3>
                            <form class="form-reply" action="">
                                <div class="form__row">
                                    <input class="form-input rounded-border" type="text" id="nama" name="nama" placeholder="Nama" required>
                                </div>
                                <div class="form__row">
                                    <input class="form-input rounded-border" type="email" id="email" name="email" placeholder="Email" required>
                                </div>
                                <div class="form__row">
                                    <textarea class="form-input rounded-border" id="komentar" name="komentar" placeholder="Komentar anda" rows="4" cols="40" required="" aria-required="true"></textarea>
                                </div>
                                <div class="form__row">
                                    <input class="btn rounded-border btn--grey" type="reset" value="Reset">
                                    <input class="btn rounded-border btn--brown" type="submit" value="Submit">
                                </div>
                            </form>
                        </section>
                    </article>
                </div>
                <div class="bzg_c" data-col="m3">
                    <?php include '_partials/sidebar.php'; ?>
                </div>
            </div>
        </div>
    </main>
    <?php include '_partials/scripts.php'; ?>
    <section id="formLogin" class="modal-box">
        <div class="modal-content">
            <div class="modal-header">
                <span class="modal-close close">×</span>
                <h3>Login</h3>
            </div>
            <div class="modal-body">
                <form class="login-form" id="loginForm" action="" novalidate="novalidate">
                    <div class="form__row">
                        <label for="">Email</label>
                        <input class="form-input form-input--block rounded-border" id="email" type="email" name="email" required>
                    </div>
                    <div class="form__row">
                        <label for="">Password</label>
                        <input class="form-input form-input--block rounded-border" id="password" type="password" name="password" required>
                    </div>
                    <div class="form__row">
                        <input type="submit" class="btn btn--brown rounded-border" value="Login">
                    </div>
                </form>
            </div>
        </div>
    </section>
</body>
</html>
