<header class="site-header">
    <div class="container">
        <div class="bzg">
            <div class="bzg_c" data-col="s6,m2">
                <h1 class="header-medium logo"><a href="./home.php">Forum anak IT</a></h1>
            </div>
            <div class="bzg_c" data-col="s1,m7">
                <div class="hidden-desktop site-nav">
                    <button class="btn btn--ghost nav-icon--search nav-icon--white-brown">
                        <i class="fa fa-search fa-fw"></i>
                    </button>
                </div>
                <form class="hidden-tablet" id="formSearch" action="" >
                    <div class="form__row form__row--block">
                        <label class="sr-only">
                            <i class="fa fa-search fa-fw"></i>
                        </label>
                        <input class="form-input form-input--block form-input--small form-input--brown rounded-border" id="" type="search" placeholder="Search" autofocus>
                        <button class="btn nav-icon--float-right btn--ghost" type="submit"><i class="fa fa-search fa-fw"></i></button>
                    </div>
                </form>
            </div>
            <div class="bzg_c" data-col="s5,m3">
                <nav class="site-nav">
                    <ul class="list-inline list-nostyle">
                        <li class="dropdown nav-collapse">
                            <a href="javascript:void(0)">
                                <span class="nav-icon"><i class="fa fa-bars fa-fw"></i></span>
                                <span class="nav-text">Categories</span>
                            </a>
                            <ul class="list-nostyle dropdown-menu">
                                <li><a href="javascript:void(0)">Linux</a></li>
                                <li><a href="javascript:void(0)">Windows</a></li>
                                <li><a href="javascript:void(0)">Mac OS</a></li>
                                <li><a href="javascript:void(0)">Android</a></li>
                                <li><a href="javascript:void(0)">iOS</a></li>
                            </ul>
                        </li>
                        <li class="nav-icon nav-icon--login">
                            <a href="#" data-modal-id="formLogin">
                                <span class="nav-icon"><i class="fa fa-sign-in fa-fw"></i></span>
                                <span class="nav-text">Login</span>
                            </a>
                        </li>
                        <li class="nav-icon nav-icon--register">
                            <a href="javascript:void(0)">
                                <span class="nav-icon"><i class="fa fa-sign-out fa-fw"></i></span>
                                <span class="nav-text">Register</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
