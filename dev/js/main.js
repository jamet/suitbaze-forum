/*! [PROJECT_NAME] | Suitmedia */

((window, document, undefined) => {

    const path = {
        css: `${myPrefix}assets/css/`,
        js : `${myPrefix}assets/js/vendor/`
    }

    const assets = {
        _objectFit      : `${path.js}object-fit-images.min.js`,
        _handlebars     : `${path.js}handlebars.min.js`,
        _bazeValidate   : `${path.js}baze.validate.min.js`
    }

    const isActive = 'is-active'

    const Site = {
        enableActiveStateMobile() {
            if ( document.addEventListener ) {
                document.addEventListener('touchstart', () => {}, true)
            }
        },

        WPViewportFix() {
            if ( '-ms-user-select' in document.documentElement.style && navigator.userAgent.match(/IEMobile/) ) {
                let style = document.createElement('style')
                let fix = document.createTextNode('@-ms-viewport{width:auto!important}')

                style.appendChild(fix)
                document.head.appendChild(style)
            }
        },

        objectFitPolyfill() {
            load(assets._objectFit).then( () => {
                objectFitImages()
            })
        },

        displayComment() {
            exist($('.list-komen')).then($listKomentar => {
                let template = $('#komentar').html()
                template = Handlebars.compile(template)
                let dataurl = $listKomentar.attr('data-content')
                console.log(dataurl)

                loadJSON(dataurl).then(data => {
                    // display comment
                    $listKomentar.append(template(data))

                    // vote comment
                    let voting = $('a.voting');
                    voting.on('click', e => {
                        let $this = $(e.currentTarget);
                        let count = Math.floor($this.siblings('.count').text());

                        if($this.hasClass('upvote')) {
                            let nilai = count + 1;
                            $this.siblings('.count').text(nilai);
                            $this.removeClass('upvote');
                            $this.parent().addClass('voted');
                            $this.addClass('votedup');
                            $this.siblings().removeClass('downvote');
                        } else if($this.hasClass('downvote'))  {
                            let nilai = count - 1;
                            $this.siblings('.count').text(nilai);
                            $this.removeClass('downvote');
                            $this.parent().addClass('voted');
                            $this.addClass('voteddown');
                            $this.siblings().removeClass('upvote');
                        } else {
                            $this.parent().addClass('voted');
                        }
                    });
                })
            }).catch( e => {} )
        },

        formValidation() {
            $('form.form-reply').bazeValidate({
                classInvalid    : 'form-input--error',
                classValid      : 'form-input--success',
            });

            $('form.login-form').bazeValidate({
                classInvalid    : 'form-input--error',
                classValid      : 'form-input--success',
            });
        },

        modalBox() {
            let modalOverlay =  '<div class="modal-overlay"></div>'

            $('a[data-modal-id]').click(function(e) {
                e.preventDefault()
                $("body").append(modalOverlay)
                $(".modal-overlay").fadeTo(500, 0.7)
                let modalBox = $(this).attr('data-modal-id')
                $('#'+modalBox).fadeIn($(this).data())
            });

            function modalClose() {
                $(".modal-box, .modal-overlay").fadeOut(500, e => {
                    $(".modal-overlay").remove()
                });
            }

            $(".modal-close").on('click', e => {
                modalClose();
            });

            $(document).on('click', '.modal-overlay', e => {
                e.preventDefault();
                modalClose();
            });

            $(document).on('keyup', e => {
                if (e.keyCode == 27) {
                    modalClose();
                }
            });
        },

        searchNav() {
            exist($('.nav-icon--search')).then($searchForm => {
                let btnClose = '<span class="form-input--overlay-close"><i class="fa fa-times fa-fw"></i></span>'

                $searchForm.on('click', e => {
                    e.preventDefault()
                    $('#formSearch').addClass('form-input--overlay')
                    // console.log('klik')
                    $('#formSearch').hide()
                    $('#formSearch').removeClass('hidden-tablet')
                    $('#formSearch').fadeIn()
                    $('#formSearch').children().append(btnClose)
                });

                $('#formSearch').on('click', '.form-input--overlay-close', event => {
                    event.preventDefault();
                    // console.log('clicked')
                    $("#formSearch").fadeOut(500, function() {
                        $('#formSearch').hide()
                        $(".modal-overlay, .form-input--overlay-close").remove()
                        $('#formSearch').removeClass('form-input--overlay')
                        $('#formSearch').addClass('hidden-tablet')
                    });
                });
            }).catch( e => {} )
        }
    }

    Promise.all([
        load(assets._handlebars),
        load(assets._bazeValidate)
    ]).then(() => {
        for (let fn in Site) {
            Site[fn]()
        }
        window.Site = Site
    })

    function exist(selector) {
        return new Promise((resolve, reject) => {
            let $elem = $(selector)

            if ( $elem.length ) {
                resolve($elem)
            } else {
                reject(`no element found for ${selector}`)
            }
        })
    }

    function load(url) {
        return new Promise((resolve, reject) => {
            Modernizr.load({
                load: url,
                complete: resolve
            })
        })
    }

    function loadJSON(url) {
        return new Promise((resolve, reject) => {
            fetch(url).then(res => {
                if ( res.ok ) {
                    resolve(res.json())
                } else {
                    reject('Network response not ok')
                }
            }).catch(e => {
                reject(e)
            })
        })
    }

})(window, document)
